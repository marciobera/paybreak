<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoanTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testStoreLoan(){
        $data = ['data' => '7a81b904f63762f00d53c4d79825420efd00f5f9, 2019-01-29T13:12:11, 10.00'];
        $response = $this->json('POST', 'api/loan/store', $data);
        $response->assertStatus(201);
        $response->assertJson([
            'error' => false,
            'message' => "Loan application stored with success."
        ]);
    }
}
