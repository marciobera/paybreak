<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThresholdTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    
    public function testShowPotentialFrauds(){
        $data = ['amount' => '100.00'];
        $response = $this->json('POST', 'api/threshold/show_potential_frauds', $data);
        $response->assertStatus(200);
        $response->assertJson(['error' => false]);
    }
}
